const BASE_URI = 'https://dev-games-backend.advbet.com/v1/ab-roulette/';
const GAME_ID = 1;

export default async function dispatcher(route, method = 'GET', payload = null) {
  const parameters = { method };
  if (payload) {
    parameters.body = payload;
  }
  const response = await fetch(`${BASE_URI}/${GAME_ID}${route}`, {
    method,
  });
  return response.json();
}
