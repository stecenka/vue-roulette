import Vue from 'vue';
import Vuex from 'vuex';
import dispatcher from '../utility/dispatcher';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    configuration: [],
    board: [],
    gameStats: [],
    gamesLog: [],
    appLog: [],
  },
  mutations: {
    fetchConfiguration(state, payload) {
      state.configuration = payload;
    },
    setBoard(state, payload) {
      state.board = payload;
    },
    fetchStats(state, payload) {
      state.gameStats = payload;
    },
    logGame(state, game) {
      state.gamesLog.push(game);
    },
    logApp(state, log) {
      state.appLog.push(`${new Date(Date.now()).toISOString()} ${log}`);
    },
  },
  actions: {
    async fetchConfiguration({ commit }) {
      const configuration = await dispatcher('/configuration');
      const board = configuration.positionToId.map((slot, index) => ({
        slot,
        color: configuration.colors[index],
      }));
      commit('fetchConfiguration', configuration);
      commit('setBoard', board);
      commit('logApp', 'GET .../configuration');
    },
    async fetchStats({ commit }, limit) {
      const data = await dispatcher(`/stats?limit=${limit}`);
      const games = data.sort((a, b) => (a.count < b.count ? 1 : 0));
      commit('fetchStats', games);
      commit('logApp', `GET .../stats?limit=${limit}`);
    },
    async logGame({ commit }, id) {
      const game = await dispatcher(`/game/${id}`);
      commit('logGame', game);
      commit('logApp', `GET .../game/${id}`);
      commit('logApp', `Result is ${game.result}`);
    },
    logApp({ commit }, log) {
      commit('logApp', log);
    },
  },
  getters: {
    board(state) {
      return state.board;
    },
    games(state) {
      return state.gameStats;
    },
    gamesLog(state) {
      return state.gamesLog;
    },
    appLog(state) {
      return state.appLog;
    },
  },
});
